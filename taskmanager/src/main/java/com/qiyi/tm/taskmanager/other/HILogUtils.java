/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qiyi.tm.taskmanager.other;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class HILogUtils {
    public static final HiLogLabel hiLogLabel=new HiLogLabel(HiLog.LOG_APP, 0x00201, "TaskManager");

    public static void debug(HiLogLabel hiLogLabel, String msg) {
        HiLog.debug(hiLogLabel,msg);
    }

    public static void error(HiLogLabel hiLogLabel, String msg) {
        HiLog.error(hiLogLabel,msg);
    }

    public static void Info(HiLogLabel hiLogLabel, String msg) {
        HiLog.info(hiLogLabel,msg);
    }

    public static void fatal(HiLogLabel hiLogLabel, String msg) {
        HiLog.fatal(hiLogLabel,msg);

    }

    public static void warn(HiLogLabel hiLogLabel, String msg) {
        HiLog.warn(hiLogLabel,msg);
    }
}
