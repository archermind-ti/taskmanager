package com.qiyi.tm.taskmanager.threadpool;

public interface IThreadIdleCallback {
    void onIdle(boolean idle);
}
