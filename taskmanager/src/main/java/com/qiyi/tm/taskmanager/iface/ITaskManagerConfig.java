package com.qiyi.tm.taskmanager.iface;

import com.qiyi.tm.taskmanager.deliver.ITracker;
import ohos.aafwk.ability.AbilityPackage;


public interface ITaskManagerConfig {
    int STRATEGY_POOL_EXECUTOR = 1;
    int STRATEGY_THREAD_GROUP = 2;

    ITaskManagerConfig enableObjectReuse(boolean b);

    ITaskManagerConfig setDefaultTimeOut(int timeOut);

    ITaskManagerConfig setLogTracker(ITracker logger);

    ITaskManagerConfig setIdleTaskOffset(int offset);

    ITaskManagerConfig setExceptionHandler(IException exceptionHandler);

    ITaskManagerConfig enableStrictModeCrash(boolean enable);

    ITaskManagerConfig enableFullLog(boolean enable);

    ITaskManagerConfig enableMemoryCleanUp(boolean enable);

    //default is 2000
    ITaskManagerConfig setLowTaskPriorityTaskMaxWaitTime(int timeInms);

    ITaskManagerConfig setWaitTimeCollectThreshold(long waitTimeCollectThreshold);

    ITaskManagerConfig setThreadPoolStrategy(int strategy);

    void initTaskManager(AbilityPackage abilityPackage);
}
