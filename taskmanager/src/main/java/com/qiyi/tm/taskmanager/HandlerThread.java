/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qiyi.tm.taskmanager;

import com.qiyi.tm.taskmanager.other.HILogUtils;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.os.ProcessManager;

public class HandlerThread extends Thread {
    int mPriority;//线程优先级
    int mTid = -1;

    EventRunner mEventRunner;
    private EventHandler mHandler;

    //指定线程的名称，默认线程优先级为DEFAULT,也是最低级的
    public HandlerThread(String name) {
        super(name);
        mPriority = ProcessManager.getThreadPriority(0);
    }

    /**
     * Constructs a HandlerThread.
     * @param name
     * @param priority The priority to run the thread at. The value supplied must be from
     * {@link } and not from java.lang.Thread.
     */
    //指定线程的名称，同时指定线程的优先级
    public HandlerThread(String name, int priority) {
        super(name);
        mPriority = priority;
    }


    //这里主要处理一些数据的初始化啊一些子线程的逻辑操作啥的，在run()方法中回调
    protected void onLooperPrepared() {
    }

    //熟悉的run()方法
    @Override
    public void run() {
        mTid = ProcessManager.getTid();
        HILogUtils.debug(HILogUtils.hiLogLabel,"mTid:"+mTid);
        EventRunner.create();
        synchronized (this) {
                mEventRunner = EventRunner.current();
                notifyAll();//唤醒等待线程

        }//设置线程优先级

            ProcessManager.setThreadPriority(mPriority);
            onLooperPrepared();
            mEventRunner.run();
            mTid = -1;
    }


    public EventRunner getLooper() {
        if (!isAlive()) {
            return null;
        }

        // If the thread has been started, wait until the looper has been created.
        synchronized (this){
            while (isAlive() && mEventRunner == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return mEventRunner;
    }



    public EventHandler getThreadHandler() {
        if (mHandler == null) {
            mHandler = new EventHandler(getLooper());
        }
        return mHandler;
    }


    //非安全的结束循环
    public boolean quit() {
        EventRunner looper = getLooper();
        if (looper != null) {
            looper.stop();
            return true;
        }
        return false;
    }


    //安全的结束循环
    public boolean quitSafely() {
        EventRunner looper = getLooper();
        if (looper != null) {
            looper.stop();
            return true;
        }
        return false;
    }

    /**
     * Returns the identifier of this thread. See Process.myTid().
     */
    public int getThreadId() {
        return mTid;
    }
}
