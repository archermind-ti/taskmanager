package com.qiyi.tm.taskmanager.threadpool;

import com.qiyi.tm.taskmanager.TaskWrapper;


public interface IThreadStrategy {
    void executeOnBackgroundThread(TaskWrapper runnable, int priority, int taskPriority);

    void onLoseThread(int priority);

    void onGainThread();

    void trigger();

    void setMaxRunningThreadCount(int count);
}

