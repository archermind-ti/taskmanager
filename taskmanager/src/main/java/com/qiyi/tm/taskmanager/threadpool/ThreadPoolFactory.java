package com.qiyi.tm.taskmanager.threadpool;

import com.qiyi.tm.taskmanager.iface.ITaskExecutor;
import com.qiyi.tm.taskmanager.iface.ITaskManagerConfig;


/**
 * this should be removed, if GroupedThreadPool is fully tested.
 */
public class ThreadPoolFactory {
    public static ITaskExecutor createExecutor(int strategy) {
        if (strategy == ITaskManagerConfig.STRATEGY_POOL_EXECUTOR) {
            return new TaskManagerExecutor();

        } else {
            return new GroupedThreadPool();
        }
    }
}
