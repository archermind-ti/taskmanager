# TaskManager

#### 简介
任务管理器是一个OHOS任务管理工具。它能够处理复杂的任务工作流程。低耦合，灵活且稳定。它支持基于关系的任务。所有任务将按照管理良好的顺序执行。它可以提交具有复杂关系的任务，例如“或依赖关系”或“与依赖关系”。同时，还支持并行任务和串行任务。

#### 功能
   1. TM.postAsync(Runnable); // 提交任务到子线程
   2. TM.postAsyncDelay(Runnable);
   3. TM.postUI(Runnable); // //提交任务到主线程执行
   4. TM.postUIDelay(Runnable, int delay);
   5. TM.postSerial(Runnable  , String groupName);//相同组名任务，按顺序执行.
   6. TM.cancelTaskByToken(Object);// 取消相同token的任务.


#### 演示
无
#### 集成
在project的build.gradle中添加mavenCentral()的引用
```
repositories {   
 	...   
 	mavenCentral()   
	 ...           
 }
```
在entry的build.gradle中添加依赖
```
dependencies { 
... 
implementation 'com.gitee.archermind-ti:TaskManager:1.0.1' 
... 
}
```
也可直接下载源码使用

#### 使用说明
*首先需要TaskManager model 添加到build.gradle 配置中加到依赖库中。
*拷贝tasks 和test 文件夹中的所有文件
* com.qiyi.tm.demo.tasks
 1.   ThreadCheckTask.java

* com.qiyi.tm.demo.test 
 1.   AsyncTest.java
 2.   CancelTest.java
 3.   DataProviderTest.java
 4.   DependantAfterTest.java
 5.   DependantTest.java
 6.   EventTaskTest.java
 7.   GrouppThreadPoolTest.java
 8.   IdelTest.java
 9.   ITest.java
 10.   MultiSyncTest.java 
 11.   NeedSyncTest.java
 12.   OrdependTest.java
 13.   ParaTest.java
 14.   ResourceUtils.java
 15.   StateCheck.java
 16.   TaskResultTest.java
 17.   Test.java
 18.   TestExecuteNow.java
 19.   TestReject.java
 20.   TestSerial.java
 21.   TickTest.java
 22.   TriggerEventTest.java
 23    UITest.java

* com.qiyi.tm
 1.   MessageMain.java
 2.    MyApplication.java    
 3.    SumChecker.java

* com.qiyi.tm.slice

 1. MainAbilitySlice.java

然后通过MainAbilitySlice中的createTest方法实例，直接调用对应的参数即可。

 createTest(4).doTest();

#### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）

#### 版本迭代
- v1.0.0   初始版本
- v1.0.1   去除了关键字
[changelog](https://gitee.com/archermind-ti/taskmanager/blob/master/changelog.md)

### 版权和许可信息
### License
TaskManager is [Apache v2.0 Licensed](https://gitee.com/archermind-ti/taskmanager/blob/master/LICENSE.txt)
