package com.qiyi.tm.demo;

import com.qiyi.tm.demo.test.*;
import org.junit.Assert;
import org.junit.Test;

public class ExampleTest {
    @Test
    public void onCancelTest() {
        Assert.assertNotNull(new CancelTest());
    }
    @Test
    public void onIdleTest() {
        Assert.assertNotNull(new IdleTest());
    }

    @Test
    public void onParaTest() {
        Assert.assertNotNull(new ParaTest());
    }
    @Test
    public void onNeedSyncTest() {
        Assert.assertNotNull(new NeedSyncTest());
    }
}
