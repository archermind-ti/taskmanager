package com.qiyi.tm.demo.test;


import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.demo.SumChecker;
import com.qiyi.tm.taskmanager.ParallelTask;
import com.qiyi.tm.taskmanager.TM;
import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.TaskRecorder;
import com.qiyi.tm.taskmanager.other.TMLog;
import ohos.global.resource.ResourceManager;


public class MultiSyncTest extends Test {

    public ResourceManager resourceManager= MyApplication.getInstance().getResourceManager();
    public MultiSyncTest(){
        super();
    }

    @Override
    public void doTest() {
        testTaskSync();
//        testEventSync();
//        testPara();
//        testParaM();
//        tsetSyncUIHybride();
    }


    /**
     * 测试executeSync 部分申明再UI 线程执行的情况
     * needSync 的并发优化必须保证 C & G 在主线程执行。
     */
    private void tsetSyncUIHybride() {

            TaskRecorder.deleteRecode(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id));
            getTask("A", 50).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSync();
            getTask("B", 100).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSync();
            getTask("C", 150).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSyncUI();
            getTask("D", 80).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSync();
            getTask("E", ResourceUtils.getResourceId(ResourceTable.String_task_3_id), 150).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSync();
            getTask("F", 80).dependOn(ResourceUtils.getResourceId((ResourceTable.String_event_vv_id))).executeSync();
            getTask("G",ResourceUtils.getResourceId(ResourceTable.String_task_4_id), 150).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSyncUI();
            getTask("H", 80).dependOn(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSync();

        new Task() {

            @Override
            public void doTask() {
                try{
                    TM.triggerEvent(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.postAsync();

    }

    /**
     * 任务与事件混合的情况下 ，验证复杂的依赖关系，执行顺序是否符合预期
     */
    private void testEventSync() {

        int p = 0;
        while (p < 1) {

            int var = (int) (75 + Math.random() * 10);
            try{
                Task taskA = getTask("A", ResourceUtils.getResourceId(ResourceTable.String_task_1_id), 50);
                Task taskB = getTask("B",ResourceUtils.getResourceId(ResourceTable.String_task_2_id), 100);
                Task taskC = getTask("C", ResourceUtils.getResourceId(ResourceTable.String_task_3_id), 150);
                Task taskD = getTask("D", ResourceUtils.getResourceId(ResourceTable.String_task_4_id), 80);
                // D 1s 后执行
                taskD.orDependOn(1000);

                //任务A 期望在任务C D 与 事件vv 完成后，立即执行
                taskA.dependOn(taskC.getTaskId(), taskD.getTaskId(), ResourceUtils.getResourceId(ResourceTable.String_event_vv_id)).executeSync();


                // B 期望再任务D完成与事件发生后，立即执行；或者等待 var 时间后执行。
                taskB.dependOn(taskD.getTaskId(),  ResourceUtils.getResourceId(ResourceTable.String_event_vv_id))
                        .orDelay(var)
                        .executeSync();


                //C 期望再事件发生的时候，立即执行
                taskC.dependOn(Integer.parseInt(resourceManager.getElement(ResourceTable.String_event_vv_id).toString()))
                        .executeSync();


                //D 期望再事件发生的时候，立即执行
                taskD.orDependOn(Integer.parseInt(resourceManager.getElement(ResourceTable.String_event_vv_id).toString())).executeSync();

                // 在1s 后，触发任务执行
                new Task() {

                    @Override
                    public void doTask() {
                        loge("DemoApp event is triggered !!!! ");

                            TM.triggerEvent(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id));

                    }
                }.postAsyncDelay(1000);

                p++;

            }catch (Exception e){
                e.printStackTrace();
            }


        }

    }


    /**
     * TestParallelTask
     */
    private void testPara() {
        final Task taskA = getTask("A", 150);
        final Task taskB = getTask("B", 100);
        final Task taskC = getTask("C", 150);
        final Task taskD = getTask("D", 80);

        new ParallelTask()
                .addSubTask(taskA)
                .addSubTask(taskB)
                .addSubTask(taskC)
                .addSubTask(taskD)
                .execute();

        log("DemoApp run tst papra end");
    }

    /**
     * 测试 AB 是否在C 完成后，能立即执行
     * 1) AB 需要在C 完成后才执行
     * 2）AB 并行执行。AB 其中之一在C 当前线程运行。
     */
    private void testTaskSync() {
        Task taskA = getTask("A", 80);
        Task taskB = getTask("B", 190);
        Task taskC = getTask("C");
        taskC.postAsync();
        taskA.dependOn(taskC).executeSync();
        taskB.dependOn(taskC).executeSync();
    }

    private void testParaM() {

        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();
        getTask(2000).postAsync();

        int p = 0;
        SumChecker checker = new SumChecker();
        while (p < 20) {
            Task taskA = getTask("A", 1).register(checker);
            Task taskB = getTask("B", time()).register(checker);
            Task taskC = getTask("C", 790).register(checker);
            Task taskD = getTask("D", 1000).register(checker);
            Task taskE = getTask("E", time()).register(checker);
            Task taskF = getTask("F", 70).register(checker);


            new ParallelTask()
                    .addSubTask(taskA)
                    .addSubTask(taskB)
                    .addSubTask(taskC)
                    .addSubTask(taskD)
                    .addSubTask(taskE)
                    .addSubTask(taskF)
                    .execute();

            checker.verriyfy();
            TMLog.d("TM_PARA",  "DONE ----");

            p++;
        }

    }


}
