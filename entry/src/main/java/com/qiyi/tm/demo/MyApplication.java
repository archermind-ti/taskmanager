package com.qiyi.tm.demo;


import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.TaskManager;
import com.qiyi.tm.taskmanager.deliver.ITracker;
import com.qiyi.tm.taskmanager.iface.ITaskManagerConfig;
import com.qiyi.tm.taskmanager.iface.ITaskStateListener;
import com.qiyi.tm.taskmanager.other.TMLog;
import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;

import static com.qiyi.tm.demo.slice.MainAbilitySlice.Tasklabel;

public class MyApplication extends AbilityPackage {
    private static final String TAG = "TM_TaskManager";
    public static    MyApplication INSTANCE;

    @Override
    public void onInitialize() {
        super.onInitialize();
    }

    public static  AbilityPackage getInstance(){
        if(INSTANCE==null){
            INSTANCE=new MyApplication();
        }
        return  INSTANCE;
    }
    @Override
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        initTaskManager();
    }

    /**
     * TM config is optional;
     */
    private void initTaskManager() {

        TaskManager.config()
                .enableFullLog(false)
                .setLogTracker(createTracker())
                .enableObjectReuse(true)
                .setDefaultTimeOut(3000)
                .enableMemoryCleanUp(true)
                .setIdleTaskOffset(100)
                .setThreadPoolStrategy(ITaskManagerConfig.STRATEGY_THREAD_GROUP)
                .initTaskManager(this);
        registerCallback();
    }

    private ITracker createTracker() {
        return new ITracker() {
            @Override
            public void track(int level, String tag, Object... msg) {
                HiLog.debug(Tasklabel, "createTracker1");
                logMessage(level == TMLog.LOG_E, tag, msg);
            }

            @Override
            public void track(Object... messages) {
                HiLog.debug(Tasklabel, "createTracker2");
                logMessage(false, TAG, messages);
            }

            @Override
            public void trackCritical(Object... messages) {
                HiLog.debug(Tasklabel, "createTracker3");
                logMessage(true, TAG, messages);
            }

            @Override
            public void printDump() {

            }

            @Override
            public void deliver(int type) {

            }

            // set true : so that will crash ,when some unoproperate use
            @Override
            public boolean isDebug() {
                // return debug mode setting
                return true;
            }
        };
    }

    private void logMessage(boolean loge, String tag, Object... msg) {
        HiLog.debug(Tasklabel, "createTracker logMessage:"+msg.length);
        if (msg != null && msg.length > 0) {
            if (msg.length == 1) {
                if (loge) {
                    HiLog.debug(Tasklabel, msg[0].toString());
                } else {
                    HiLog.debug(Tasklabel, msg[0].toString());
                }
            } else {
                StringBuilder builder = new StringBuilder();
                for (Object o : msg) {
                    builder.append(o.toString());
                    builder.append(" ");
                }

                if (loge) {
                    HiLog.debug(Tasklabel, TAG+ builder.toString());
                } else {
                    HiLog.debug(Tasklabel, TAG+ builder.toString());
                }

            }
        }
    }

    private void registerCallback() {
        TaskManager.getInstance().registerTaskStateListener(new ITaskStateListener() {
            @Override
            public void onTaskStateChange(Task task, int state) {
                if (state == ITaskStateListener.ON_START) {
                    HiLog.debug(Tasklabel, "started <<<<<<< " + task);
                } else if (state == ITaskStateListener.ON_FINISHED) {
                    HiLog.debug(Tasklabel, "               Finished !!!!!!" + task);
                } else {
                    HiLog.debug(Tasklabel, "onTaskStateChange Canceled: " + task);
                }
            }
        });

    }
}
