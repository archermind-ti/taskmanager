package com.qiyi.tm.demo.slice;


import com.qiyi.tm.demo.test.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


public class MainAbilitySlice extends AbilitySlice {

    private  DirectionalLayout directionalLayout = new DirectionalLayout(getContext());
    public static final HiLogLabel Tasklabel = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TaskManager");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        init();
         super.setUIContent(directionalLayout);
        createTest(4).doTest();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
    public void init(){

        //Declaration layout
        DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_PARENT);
        directionalLayout.setLayoutConfig(config);
        directionalLayout.setOrientation(Component.VERTICAL);
        ShapeElement dirLayoutshElement = new ShapeElement();
        dirLayoutshElement.setRgbColor(new RgbColor(0,255, 255));
        directionalLayout.setBackground(dirLayoutshElement);

        Text textView = new Text(this);
        textView.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        textView.setHeight(ComponentContainer.LayoutConfig.MATCH_PARENT);
        ShapeElement background = new ShapeElement();
        background.setRgbColor(new RgbColor(0, 255, 255));
        background.setCornerRadius(25);
        textView.setBackground(background);
        textView.setTextSize(100);
        textView.setText("TaskManager Demo");
//        textView.setText(ResourceUtils.getStringResourceId(ResourceTable.String_App_name_text));
        textView.setTextColor(Color.BLACK);
        textView.setTextAlignment(TextAlignment.CENTER);
        directionalLayout.addComponent(textView);
        directionalLayout.setAlignment(17);
        directionalLayout.setMarginTop(100);
        setUIContent(directionalLayout);
    }


            private Test createTest(int id) {
                switch (id) {
                    case 1:
                        //test to post a task to run in background thread
                        HiLog.debug(Tasklabel, "======MainAbility createTest 1.");
                        return new AsyncTest();

                    case 2:
                        //test to cancel a task by id or token
                        HiLog.debug(Tasklabel, "AsyncTest case 2");
                        return new CancelTest();
                    case 3:
                        //test for api: DependantAfterTest
                        HiLog.debug(Tasklabel, "AsyncTest case 3");
                        return new DependantAfterTest();
                    case 4:
                        //test for task dependency relationship
                        HiLog.debug(Tasklabel, "AsyncTest case 4");
                        return new DependantTest();
                    case 5:
                        HiLog.debug(Tasklabel, "AsyncTest case 5");
                        return new EventTaskTest();
                    case 6:
                        HiLog.debug(Tasklabel, "AsyncTest case 6");
                        return new IdleTest();
                    case 7:
                        HiLog.debug(Tasklabel, "AsyncTest case 7");
                        return new MultiSyncTest();
                    case 8:
                        HiLog.debug(Tasklabel, "AsyncTest case 8");
                        return new NeedSyncTest();
                    case 9:
                        HiLog.debug(Tasklabel, "AsyncTest case 9");
                        return new OrdependTest();
                    case 10:
                        HiLog.debug(Tasklabel, "AsyncTest case 10");
                        return new ParaTest();
                    case 11:
                        HiLog.debug(Tasklabel, "AsyncTest case 11");
                        return new TestExecuteNow();
                    case 12:
                        HiLog.debug(Tasklabel, "AsyncTest case 12");
                        return new TaskResultTest();
                    case 13:
                        HiLog.debug(Tasklabel, "AsyncTest case 13");
                        return new TestReject();
                    case 14:
                        HiLog.debug(Tasklabel, "AsyncTest case 14");
                        return new TestSerial();
                    case 15:
                        HiLog.debug(Tasklabel, "AsyncTest case 15");
                        return new TriggerEventTest();
                    case 16:
                        HiLog.debug(Tasklabel, "AsyncTest case 16");
                        return new GroupThreadPoolTest();

                    default:
                        HiLog.debug(Tasklabel, "AsyncTest case default");
                        return new AsyncTest();
                }
            }

}
