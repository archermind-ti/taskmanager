package com.qiyi.tm.demo.test;


import com.qiyi.tm.demo.slice.MainAbilitySlice;
import ohos.hiviewdfx.HiLog;

public class AsyncTest extends Test {
    public AsyncTest() {
        super();
        HiLog.debug(MainAbilitySlice.Tasklabel, "======MainAbility AsyncTest");
    }

    @Override
    public void doTest() {
        getTask("AsyncTest").postAsyncDelay(1000);
}
}
