package com.qiyi.tm.demo.test;


import com.qiyi.tm.taskmanager.ParallelTask;
import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.other.TMLog;

public class TaskResultTest extends Test {
    public TaskResultTest(){
        super();
    }
    @Override
    public void doTest() {
        final int ar[] = new int[3];
        new ParallelTask().addSubTask(new Task() {
            @Override
            public void doTask() {
                setResult(1);
            }
        })
                .addSubTask(new Task() {
                    @Override
                    public void doTask() {
                        setResult(2);
                    }
                })
                .addSubTask(new Task() {
                    @Override
                    public void doTask() {
                        setResult(3);
                    }
                })
                .setCallBackOnSubTaskFinished(new ParallelTask.TaskResultCallback() {
                    @Override
                    public void onResult(Task task, Object var, int index) {
                        TMLog.d(TAG,"index " + index + " " + var);
                        ar[index] = (int) var;
                    }
                })
                .execute();
        TMLog.d(TAG, + ar[0] + " " + ar[1] + " " + ar[2]);
    }
}
