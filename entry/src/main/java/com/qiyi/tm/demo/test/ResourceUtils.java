/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qiyi.tm.demo.test;

import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.taskmanager.other.HILogUtils;
import ohos.global.resource.ResourceManager;

public class ResourceUtils {

    private static  ResourceManager resManager= MyApplication.getInstance().getResourceManager();
    /**
     * 通过资源Id 获取String id 资源
     * @param resId
     * @return
     */
    public  static int getResourceId(int resId){

        try{
            if(resManager!=null && resManager.getElement(resId)!=null) {
                return Integer.parseInt(resManager.getElement(resId).toString());
            }
        }catch (Exception e){
            e.printStackTrace();
            HILogUtils.error(HILogUtils.hiLogLabel,"exception:"+e.getMessage());
        }
        return 0;
    }

    /**
     * 通过资源Id 查找String
     * @param resId
     * @return
     */
    public  static String  getStringResourceId(int resId){

        try{
            if(resManager!=null && resManager.getElement(resId)!=null) {
                return   resManager.getElement(resId).getString();

            }
        }catch (Exception e){
            e.printStackTrace();
            HILogUtils.error(HILogUtils.hiLogLabel,"exception:"+e.getMessage());
        }
        return  "";
    }
}
