package com.qiyi.tm.demo.test;


import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.taskmanager.Task;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;

import static com.qiyi.tm.demo.slice.MainAbilitySlice.Tasklabel;


/**
 * 测试各种或逻辑是否正常执行
 */
public class OrdependTest extends Test {

    public OrdependTest(){
        HiLog.debug(Tasklabel, "OrdependTest Construction");
    }
    @Override
    public void doTest() {
//        test2();
        test1();
    }


    /**
     * 任务3 在任务1 或者 任务2 完成后，执行
     */
    private void test1() {
        try{
            ohos.global.resource.ResourceManager resManager = MyApplication.getInstance().getAbilityPackageContext().getResourceManager();
            Task task1 = new Task("task1", ResourceUtils.getResourceId(ResourceTable.String_tash_v1_id)) {
                @Override
                public void doTask() {

                }
            };
            //getTask("task1");
            Task task2 = new Task("task2",  ResourceUtils.getResourceId(ResourceTable.String_tash_v1_id)) {
                @Override
                public void doTask() {

                }
            };
            getTask("task2");
            Task task3 = getTask("task3");
            task1.postUIDelay(3000);
            task2.postAsyncDelay(500);
            task3.dependOn(task1)
                    .orDependOn(task2)
                    .postAsync();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void test2(Context context) throws  Exception {
        String tash_v1 =  context.getResourceManager().getElement(ResourceTable.String_tash_v1_id).getString();
        Task task1 = new Task("task1", Integer.parseInt(tash_v1)) {
            @Override
            public void doTask() {

            }
        };
        getTask("task1");
        Task task2 = new Task("task2", Integer.parseInt(tash_v1)) {
            @Override
            public void doTask() {

            }
        };
//        getTask("task2");

        Task task3 = getTask("task3");

        task1.postUIDelay(3000);
        task2.postAsyncDelay(3500);
        task3.dependOn(task1.getTaskId())
                .orDependOn(task2.getTaskId())
//                .postAsyncDelay(6000);
                .postAsync();
    }
}
