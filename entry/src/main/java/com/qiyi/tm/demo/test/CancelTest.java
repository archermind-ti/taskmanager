package com.qiyi.tm.demo.test;


import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.taskmanager.TM;
import com.qiyi.tm.taskmanager.Task;

/**
 * 验证任务是否能被正常取消
 */
public class CancelTest extends Test {

    public CancelTest(){
        super();
    }

    @Override
    public void doTest() {

            Task task = new Task("CancelTest-task-1",ResourceUtils.getResourceId(ResourceTable.String_task_1_id)) {
                @Override
                public void doTask() {
                }
            };

            task.setToken(this);
            task.postUIDelay(1000);

            Task task2 = new Task("CancelTest-task-1",ResourceUtils.getResourceId(ResourceTable.String_task_2_id)) {
                @Override
                public void doTask() {

                }
            };
            task2.postUIDelay(1000);
            task2.setToken(this);

            Task task1 = getTask("1t");
            task1.dependOn(task.getTaskId())
                    .orDependOn(task2.getTaskId()).postAsync();
            TM.cancelTaskByToken(this);
            TM.cancelTaskById(task.getTaskId());

    }
}
