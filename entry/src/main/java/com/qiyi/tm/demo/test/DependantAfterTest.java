package com.qiyi.tm.demo.test;


import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.other.HILogUtils;
import ohos.global.resource.ResourceManager;

public class DependantAfterTest extends Test {

    public DependantAfterTest(){
        super();

    }

    @Override
    public void doTest() {
        Task task1 = new Task("DependantAfterTest", ResourceUtils.getResourceId(ResourceTable.String_task_1_id)) {
            @Override
            public void doTask() {
                // do sth
            }
        };
        task1.postAsync();
        HILogUtils.debug(HILogUtils.hiLogLabel,"taskId:"+task1.getTaskId());


        Task task2 = getTask("task2",task1.getTaskId());
        task2.postAsyncDelay(1000);
        HILogUtils.debug(HILogUtils.hiLogLabel,"task2Id:"+task2.getTaskId());

        // task would be hold for 300ms, then check task1 state , if its already finished , then  wait 1000ms to run;
       Task task3 = getTask("task3",task1.getTaskId());
        task3.postAsyncDelay(1000);
    }


}
