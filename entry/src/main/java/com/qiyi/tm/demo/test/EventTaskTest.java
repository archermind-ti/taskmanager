package com.qiyi.tm.demo.test;


import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.taskmanager.EventTask;
import com.qiyi.tm.taskmanager.TM;
import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.TickTask;
import com.qiyi.tm.taskmanager.other.HILogUtils;
import ohos.global.resource.ResourceManager;


/**
 * 测试各种事件
 * 1, 自定义事件
 * 2, id 事件
 * 3, 事件携带数据
 */
public class EventTaskTest extends Test {

    public ResourceManager resourceManager=MyApplication.getInstance().getResourceManager();

    public EventTaskTest(){
        super();
    }

    @Override
    public void doTest() {
//        triggerEventTask();
//        triggerEvent();
//        triggerSelfDefinedEvent();
        triggerGlobalEvent();


    }


    /**
     * 全局的任务 trigger
     */
    private void triggerGlobalEvent() {
        short groupid=1;
        new EventTask() {

            @Override
            public void onEvent(int eventId, Object msg) {
                log("gochar globacl evt " + msg);
            }
        }.setGroupId(groupid).registerEvents(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id))
                .postAsync();


        new TickTask() {
            @Override
            public void onTick(int loopTime) {
                try{
                    TM.triggerEvent(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id) , 5);
                }catch (Exception e){
                    e.printStackTrace();
                    HILogUtils.error(HILogUtils.hiLogLabel,"Exception:"+e.getMessage());
                }
            }
        }.setIntervalWithFixedDelay(100)
                .setMaxLoopTime(7).post();


    }

    private void triggerEventTask() {
        Task task = getTask("event task");
        task.postPending();
        TM.triggerEventTaskFinished(task.getTaskId());
    }

    /**
     * id define
     */
    private void triggerEvent() {
        Task task = new Task("alashk") {
            @Override
            public void doTask() {
                    Integer tye = getData(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id), Integer.class);
//                log("gochar: " + tye);

            }
        };

            task.dependOn( ResourceUtils.getResourceId(ResourceTable.String_event_vv_id));
            task.postAsync();

        new Task() {
            @Override
            public void doTask() {
                    TM.triggerEvent(ResourceUtils.getResourceId(ResourceTable.String_event_vv_id), new Integer(2));
            }
        }.postUIDelay(1000);

    }

    int var = 0;

    /**
     *
     */
    private void triggerSelfDefinedEvent() {


        new EventTask() {

            @Override
            public void onEvent(int eventId, Object msg) {

                log("gochar: evt " + eventId + " msg: " + msg);

                if (eventId == 30) {
                    unregister();
                }

            }
        }.registerGroupedEvents(this, 1, 2, 3)
                .post();


        new TickTask() {

            @Override
            public void doTask() {
                var++;
                int p = var % 4;
                TM.triggerEvent(EventTaskTest.this, p, new Integer(211));

            }

            @Override
            public void onTick(int loopTime) {

            }
        }.setIntervalWithFixedDelay(1000)
                .setMaxLoopTime(100)
                .postUI();

    }

}
