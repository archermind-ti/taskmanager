package com.qiyi.tm.demo.test;



import com.qiyi.tm.demo.MyApplication;
import com.qiyi.tm.demo.ResourceTable;
import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.other.TMLog;
import ohos.global.resource.ResourceManager;


public class DependantTest extends Test {
    public ResourceManager resourceManager=MyApplication.getInstance().getResourceManager();

    public DependantTest(){
        super();
    }

    @Override
    public void doTest() {
        taskiii();
//        taskDDD();
//        orDependant();
//        orDependant2();
    }

    protected void assetThread(boolean ui) {
        // do nothing
    }

    /**
     * test task depend on wrong id : to tasks are of same id
     */
    private void taskiii() {
        try{
            ResourceManager resourceManager= MyApplication.getInstance().getResourceManager();
            Task task1 = getTask("task1", Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_1_id).toString()), 100)
                    .setCallBackOnFinished(new Task.TaskResultCallback() {
                        @Override
                        public void onResultCallback(Task task, Object var) {
                            TMLog.d(TAG, "XXX");
                        }
                    });
            task1.postAsyncDelay(2010);


            Task task2 = getTask("task2",Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_2_id).toString()) , 200);
            task2.postUIDelay(2000);

            Task task3 = getTask("task3");
            task3.dependOn(task1.getTaskId(), task2.getTaskId());
            task3.executeSyncUI();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * direct task dependant
     * Task 3 should be executed on UI thread
     */
    private void taskDDD() {
        Task task1 = getTask("task1");
        task1.postAsyncDelay(2010);
        Task task2 = getTask("task2");
        task2.postUIDelay(2000);
        Task task3 = getTask("task3");
        task3.dependOn(task1, task2);
        task3.executeSyncUI();
    }


    /**
     * 当Ordelay 设置的时间 < 依赖任务完成的时间时候
     */
    private void orDependant() {
        Task task1 = getTask("task11");
        task1.postAsyncDelay(2010);
        getTask("task22").delayAfter(200, task1);
        try{
            new Task("ordepend") {
                @Override
                public void doTask() {
                    // your task
                }
            }.dependOn(1, 2) // not running
                    .dependOn(Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_1_id).toString()), Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_2_id).toString())) // not running
                    .orDependOn(Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_3_id).toString(), Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_4_id).toString())))// not running
                    .orDependOn(3,4) // not running
                    .orDelay(8000) // run after 2000ms
                    .post();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 当Ordelay 设置的时间> 依赖任务完成的时间时候
     */
    private void orDependant2() {
        Task task1 = getTask("task11");
        task1.postAsyncDelay(2010);
        Task task2 = getTask("task22").delayAfter(200, task1);
        try{
            new Task("ordepend") {
                @Override
                public void doTask() {
                    // your task
                }
            }.dependOn(task1, task2) // not running
                    .orDependOn(Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_3_id).toString()), Integer.parseInt(resourceManager.getElement(ResourceTable.String_task_4_id).toString())) // not running
                    .orDependOn(3,4)
                    .orDelay(8000) // run after 2000ms
                    .post();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
