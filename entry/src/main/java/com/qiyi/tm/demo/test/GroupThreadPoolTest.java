package com.qiyi.tm.demo.test;

import com.qiyi.tm.taskmanager.Task;
import com.qiyi.tm.taskmanager.TickTask;


/**
 * 验证在 任务闲置后，过一段时间再启动大量并发任务 TM 的表现
 */
public class GroupThreadPoolTest extends Test{
    public GroupThreadPoolTest(){
        super();
    }
    @Override
    public void doTest() {
        new TestExecuteNow().doTest();

        new Task(){

            @Override
            public void doTask() {
                new TestExecuteNow().doTest();
            }
        }.postAsyncDelay(60000);


        new TickTask(){

            @Override
            public void onTick(int loopTime) {
            }
        }.setIntervalWithFixedDelay(5000)
                .postAsync();
    }
}
